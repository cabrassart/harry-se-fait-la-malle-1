def monnaie_b(a_rendre):
    en_caisse = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    rendu = {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 2: 0}
    for valeur in en_caisse.keys():
        if a_rendre - valeur >= 0:
            while en_caisse[valeur] != 0:
                a_rendre -=  valeur
                en_caisse[valeur] -= 1
                rendu[valeur] += 1
    if a_rendre > 0:
        a_rendre -=  valeur
        en_caisse[valeur] -= 1
        rendu[valeur] += 1

    return rendu
    
    
    
def reponse_b(somme_a_rendre):
    argent = 0
    resultat = monnaie_b(somme_a_rendre)
    texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre: '
    for i in resultat.keys():
        if resultat[i] >= 1 :
            if i > 2:
                texte += f"{resultat[i]} billet(s) de {i} "
            else :
                texte += f"{resultat[i]} pièce(s) de {i} "
        else :
            argent += 1
    if argent == len(resultat.keys):
        texte += 'rien'
    return texte
    
    
    
    
    

demande = int(input("Si vous voulez choisir votre somme a rendre, tapez 1"
                    "Si vous voulez voir les exemples tapez 2"))
if demande == 1:
    a_rendre = int(input("Combien dois-je vous rendre ?"))
    print(reponse_b(a_rendre))
    demande = int(input("Si vous voulez voir les exemples tapez 2"))

if demande == 2:
    print(reponse_b(0))
    print(reponse_b(8))
    print(reponse_b(62))
    print(reponse_b(231))
    print(reponse_b(497))
    print(reponse_b(842))