def monnaie_b(a_rendre):
    en_caisse = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    rendu = {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 2: 0}
    for valeur in en_caisse.keys():
        if a_rendre - valeur >= 0:
            while en_caisse[valeur] != 0:
                a_rendre -=  valeur
                en_caisse[valeur] -= 1
                rendu[valeur] += 1
    if a_rendre > 0:
        a_rendre -=  valeur
        en_caisse[valeur] -= 1
        rendu[valeur] += 1

    return rendu


a_rendre = int(input("Combien dois-je vous rendre ?"))
resultat = monnaie_b(a_rendre)
print("Je dois vous rendre :", end = '')
for i in resultat.keys():
    if resultat[i] >= 1 :
        if i > 2:
            print(f",{resultat[i]} billet(s) de {i} ", end = '')
        else :
            print(f",{resultat[i]} pièce(s) de {i} ", end = '')

