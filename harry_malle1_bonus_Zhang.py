# encoding ~ utf-8 ~
from tkinter import *


def monnaie_a(somme_a_rendre):
    """
    permet de definir le nombre de billet qu'il faut rendre.

    Entrée : un entier qui représente la somme que l'on doit rendre
    Sortie : un dictionnaire comportant pour clés les noms des billets
             et pour valeurs le nombre de billet qu'il faut

    Version finale
    Lou, Clémence, Mayline
    """

    dico_billet = {500: 0, 200: 0, 100: 0, 50: 0,
                   20: 0, 10: 0, 5: 0, 2: 0, 1: 0}
    while somme_a_rendre != 0:
        for valeur in dico_billet.keys():
            while somme_a_rendre >= valeur:
                dico_billet[valeur] += somme_a_rendre // valeur
                somme_a_rendre -= (somme_a_rendre // valeur) * valeur
    return dico_billet


def reponse_a(somme_a_rendre):
    """
    Permet d'afficher le nombre de billet qu'il faut rendre dans des phrases

    Entrée: un etier qui représente la somme que l'on doit rendre
    Sortie: une chaine de caractère exprimant le nombre de billet
            que l'on doit rendre

    Version finale
    Lou, Clémence, Mayline
    """

    argent = 0
    resultat = monnaie_a(somme_a_rendre)
    texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre:\n '
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i > 2:
                texte += f"{resultat[i]} billet(s) de {i}\n"
            else:
                texte += f"{resultat[i]} pièce(s) de {i}\n"
        else:
            argent += 1
    if argent == 9:
        texte += 'rien'
    print(texte)
    return texte


def monnaie_b(a_rendre):
    """
    Permet de donner le nombre de billets qu'il faut rendre

    Entrée: un entier représantant la somme à rendre
    Sortie: un dictionnaire avec le nombre de billet
            qu'il faut et un boolén indiquant si on a
            arrondit au supérieur ou non.

    Version finale
    Lou, Clémence, Mayline

    """
    arrondit = False
    en_caisse = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    rendu = {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 2: 0}
    if a_rendre > 590:
        print("C'est trop je ne pourrais pas tous rendre mais: ")
    for valeur in en_caisse.keys():
        while a_rendre - valeur >= 0 and en_caisse[valeur] != 0:
                a_rendre -= valeur
                en_caisse[valeur] -= 1
                rendu[valeur] += 1
    if a_rendre == 1 and en_caisse[2] != 0:
        a_rendre -= 1
        en_caisse[2] -= 1
        rendu[2] += 1
        arrondit = True
    elif 100 > a_rendre > 1 and en_caisse[100] != 0:
        a_rendre -= 100
        en_caisse[100] -= 1
        rendu[100] += 1
        en_caisse[50] -= 1
        rendu[50] -= 1
        en_caisse[20] -= 1
        rendu[20] -= 1
        en_caisse[10] -= 1
        rendu[10] -= 1
        en_caisse[2] -= 5
        rendu[2] -= 5
        arrondit = True
    return rendu, arrondit


def reponse_b(somme_a_rendre):
    """
    Permet de donner une réponse avec le nombre de billet qu'il faaut rendre

    Entrée: un entier qui correspond à la somme à rendre
    Sortie: Une chaine de caractère qui affiche le nombre
            de billet qu'il faut ou que l'on ne peut pas rendre

    Version finale
    Lou, Clémence, Mayline

    """
    argent = 0
    resultat = monnaie_b(somme_a_rendre)
    arrondit = resultat[1]
    resultat = resultat[0]
    texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre:\n'
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i > 2:
                texte += f"{resultat[i]} billet(s) de {i}\n"
            else:
                texte += f"{resultat[i]} pièce(s) de {i}\n"
        else:
            argent += 1
    if arrondit and somme_a_rendre <= 590:
        texte += "\nJe n'avait pas les billets nécessaires pour vous"
        texte += "rendre la monnaie exacte je vous ait donc rendu plus.\n"
    if argent == 6:
        texte += 'rien'
    print(texte)
    return texte


def monnaie_c(gallions, mornilles, noises):
    """
    Permet de donner le nombre de gallions, de noises et de morilles
    qu'il faut rendre tout en les convertissant

    Entrée: 3 entier représentant le nombre de gallions
            de mornilles et de noises
    Sortie: un dictionaire avec comme clé mornilles, gallions
            et noises et le nombre qu'il faut

    Version finale
    Lou, Clémence, Mayline
    """
#   1 gallion vaut 17 mornilles , un mornille vaut 29 noises
    monnaie_a_rendre = {'gallions': 0, 'mornilles': 0, 'noises': 0}
    if noises >= 29:   # conversion noises en mornilles
        mornilles += noises // 29
        noises = noises % 29
    if mornilles >= 17:   # conversion mornilles en gallions
        gallions += mornilles // 17
        mornilles = mornilles % 17

    monnaie_a_rendre['gallions'] = gallions

    monnaie_a_rendre['mornilles'] = mornilles

    monnaie_a_rendre['noises'] = noises

    return monnaie_a_rendre


def reponse_c(gallions, mornilles, noises):
    """
    Permet d'afficher sous la forme d'un texte ce que l'on doit rendre

    Entrée: trois entiers correspondant au nombre de gallions,
            de mornilles et de noises que l'on doit rendre
    Sortie: une chaine de caractère expremimant ce que l'on doit rendre

    Version finale
    Lou, Clémence, Mayline
    """
    resultat = monnaie_c(gallions, mornilles, noises)
    texte = f"Je vous dois {gallions} gallions, {mornilles} "
    texte += f"mornilles et {noises} noises.\nJe vais donc vous rendre :\n"
    argent = 0
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i == 'gallions':
                texte += f'{resultat[i]} gallions\n'
            elif i == 'mornilles':
                texte += f'{resultat[i]} mornilles\n'
            else:
                texte += f'{resultat[i]} noises.\n'
        else:
            argent += 1
    if argent == 3:
        texte += 'Rien'
    print(texte)
    return texte


def exemple_a():
    """
    Permet d'executer les exemple donner dans le cahier des charges
    de la partie 1 et de les afficher
    """
    reponse_a(0)
    print()
    reponse_a(60)
    print()
    reponse_a(63)
    print()
    reponse_a(231)
    print()
    reponse_a(899)
    print()


def exemple_b():
    """
    Permet d'executer les exemple donner dans le cahier
    des charges de la partie 2 et de les afficher
    """
    reponse_b(0)
    print()
    reponse_b(8)
    print()
    reponse_b(62)
    print()
    reponse_b(231)
    print()
    reponse_b(497)
    print()
    reponse_b(899)
    print()


def exemple_c():
    """
    Permet d'executer les exemple donner dans le cahier
    des charges de la dernière partie et de les affciher
    """
    reponse_c(0, 0, 0)
    print()
    reponse_c(0, 0, 654)
    print()
    reponse_c(0, 23, 78)
    print()
    reponse_c(2, 11, 9)
    print()
    reponse_c(7, 531, 451)
    print()


def question_ab():
    """
    Demande la somme a rendre pour le magasin a ou b.
    """
    demande = int(input("Combien dois-je vous rendre ?"))
    return demande


def question_c():
    """
    Demande la somme a rendre pour le magasin c.
    """
    gallions = int(input('Combien de gallions dois-je vous rendre ?'))
    mornilles = int(input("Combien de mornilles dois-je vous rendre ?"))
    noises = int(input('Combien de noises dois-je vous rendre ?'))
    return gallions, mornilles, noises


def create_a():
    """
    Ouvre la fenetre pour le magasin a (Fleury & Bott) 
    avec deux boutons.
    """
    widget = Toplevel(fenetre)
    widget.title("Fleury & Bott")
    widget.config(background='#C3B8E6')
    frame_a = Frame(widget, bg='#C3B8E6')
    button_ex = Button(frame_a, text="exemple",  font=("Grabriola",30), bg='white', fg='#3D2784', command=exemple_a)
    button_ex.pack()
    button_choix = Button(frame_a, text="choisir la somme à rendre",  font=("Grabriola",30), bg='white', fg='#3D2784', command=reponse_a(question_ab)
    button_choix.pack()
    label = Label(frame_a, text="regardez les résultats dans l'IDE", font=("Gabriola",30), bg='#C3B8E6', fg='#3D2784')
    label.pack()
    frame_a.pack(expand=YES)


def create_b():
    """
    Ouvre la fenetre pour le magasin b (Madame Guipure) 
    avec deux boutons.
    """
    widget = Toplevel(fenetre)
    widget.title("Madame Guipure")
    widget.config(background='#C3B8E6')
    frame_b = Frame(widget, bg='#C3B8E6')
    button_ex = Button(frame_b, text="exemple",  font=("Grabriola",30), bg='white', fg='#3D2784', command=exemple_b)
    button_ex.pack()
    button_choix = Button(frame_b, text="choisir la somme à rendre",  font=("Grabriola",30), bg='white', fg='#3D2784', command=reponse_b(question_ab)
    button_choix.pack()
    label = Label(frame_b, text="regarder les résultats dans l'IDE", font=("Gabriola",30), bg='#C3B8E6', fg='#3D2784')
    label.pack()
    frame_b.pack(expand=YES)


def create_c():
    """
    Ouvre la fenetre pour le magasin c (Ollivander) 
    avec deux boutons.
    """
    widget = Toplevel(fenetre)
    widget.title("Ollivander")
    widget.config(background='#C3B8E6')
    frame_c = Frame(widget, bg='#C3B8E6')
    button_ex = Button(frame_c, text="exemple",  font=("Grabriola",30), bg='white', fg='#3D2784', command=exemple_c)
    button_ex.pack()
    button_choix = Button(frame_c, text="choisir la somme à rendre",  font=("Grabriola",30), bg='white', fg='#3D2784', command=reponse_c(question_c))
    button_choix.pack()
    label = Label(frame_c, text="regarder les résultats dans l'IDE", font=("Gabriola",30), bg='#C3B8E6', fg='#3D2784')
    label.pack()
    frame_c.pack(expand=YES)
    

fenetre = Tk()
#creer une fenetre

#personnaliser cette fenetre
fenetre.title("Rendu de monnaie")
fenetre.geometry("900x600")
fenetre.minsize(480, 400)
fenetre.config(background='#C3B8E6')

#creer une boite
frame = Frame(fenetre, bg='#C3B8E6')

#creer une autre boite
frame_1 = Frame(fenetre, bg='#C3B8E6')

#ajouter un titre
label_title = Label(frame, text="Harry se fait la malle !", font=("Gabriola",50), bg='#C3B8E6', fg='#3D2784')
label_title.pack()

#ajouter un autre texte
label_subtitle = Label(frame, text="Voici les sommes à rendre chez :", font=("Gabriola",30), bg='#C3B8E6', fg='#3D2784')
label_subtitle.pack()

#ajouter un bouton
button_a = Button(frame_1, text="Fleury & Bott", font=("Grabriola",30), bg='white', fg='#3D2784', command = create_a)
button_a.pack(pady=25, fill=X)

#ajouter 2eme bouton
button_b = Button(frame_1, text="Madame Guipure", font=("Grabriola",30), bg='white', fg='#3D2784', command = create_b)
button_b.pack(pady=25, fill=X)

#ajouter 3eme bouton
button_c = Button(frame_1, text="Ollivander", font=("Grabriola",30), bg='white', fg='#3D2784', command = create_c)
button_c.pack(pady=25, fill=X)

#ajouter la boite
frame.pack(expand=YES)

#ajouter la 2eme boite
frame_1.pack(expand=YES)


fenetre.mainloop()