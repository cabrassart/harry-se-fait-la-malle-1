demande = int(input("Si vous voulez choisir votre somme a rendre, tapez 1"
                    "Si vous voulez voir les exemples tapez 2"))
if demande == 1:
    somme_a_rendre = int(input("Combien dois-je vous rendre ?"))
    def monnaie_a (somme_a_rendre) :
        dico_billet = {500: 0, 200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 5: 0, 2: 0, 1: 0}
        while somme_a_rendre != 0:
            for valeur in dico_billet.keys() :
                while somme_a_rendre >= valeur :
                    for i in range(somme_a_rendre // valeur) :
                        dico_billet[valeur] += 1
                    somme_a_rendre -= (somme_a_rendre // valeur) * valeur
        return dico_billet

    def reponse(somme_a_rendre):
        argent = 0
        resultat = monnaie_a(somme_a_rendre)
        texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre: '
        for i in resultat.keys():
            if resultat[i] >= 1 :
                if i > 2:
                    texte += f"{resultat[i]} billet(s) de {i} "
                else :
                    texte += f"{resultat[i]} pièce(s) de {i} "
            else :
                argent += 1
        if argent == 9:
            texte += 'rien'
        return texte
    print(reponse(somme_a_rendre))
    demande = int(input("Si vous voulez voir les exemples tapez 2"))

if demande == 2:
    print(reponse(0))
    print(reponse(60))
    print(reponse(63))
    print(reponse(231))
    print(reponse(899))