# -*- coding: utf-8 -*-
from tkinter import *

#creer une fenetre
fenetre = Tk()

#personnaliser cette fenetre
fenetre.title("Rendu de monnaie")
fenetre.geometry("900x600")
fenetre.minsize(480, 400)
fenetre.config(background='#C3B8E6')

#creer une boite
frame = Frame(fenetre, bg='#C3B8E6')

#creer une autre boite
frame_1 = Frame(fenetre, bg='#C3B8E6')

#ajouter un texte
label_title = Label(frame, text="Harry se fait la malle !", font=("Gabriola",50), bg='#C3B8E6', fg='#3D2784')
label_title.pack()

#ajouter un autre texte
label_subtitle = Label(frame, text="Voici les sommes à rendre chez :", font=("Gabriola",30), bg='#C3B8E6', fg='#3D2784')
label_subtitle.pack()

#ajouter un bouton
button_a = Button(frame_1, text="Fleury & Bott", font=("Grabriola",30), bg='white', fg='#3D2784',)
button_a.pack(pady=25, fill=X)

#ajouter 2eme bouton
button_b = Button(frame_1, text="Madame Guipure", font=("Grabriola",30), bg='white', fg='#3D2784',)
button_b.pack(pady=25, fill=X)

#ajouter 3eme bouton
button_c = Button(frame_1, text="Ollivander", font=("Grabriola",30), bg='white', fg='#3D2784',)
button_c.pack(pady=25, fill=X)

#ajouter la boite
frame.pack(expand=YES)

#ajouter la 2eme boite
frame_1.pack(expand=YES)

#afficher
fenetre.mainloop()

