# encoding ~ utf-8 ~
"""
Programme répondant au projet Malle_1 de la matière NSI, permet de calculer
et de donner le nombre de billets qu'il faut rendre selon les spécificités
des magasins. Ce programme permet dans chaque magasin de choisir la
somme à rendre mais aussi de montrer des exemples.

ABRASSART Clémence, LAPAUW Lou, ZHANG Mayline
Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
17/12/2021
"""

#Définition des fonctions
def monnaie_a(somme_a_rendre):
    """
    Permet de définir le nombre de billets qu'il faut rendre.

    Entrée : un entier qui représente la somme que l'on doit rendre
    Sortie : un dictionnaire comportant pour clés les noms des billets
             et pour valeurs le nombre de billets qu'il faut

    Version finale
    Lou, Clémence, Mayline
    """

    dico_billet = {500: 0, 200: 0, 100: 0, 50: 0,
                   20: 0, 10: 0, 5: 0, 2: 0, 1: 0}
    while somme_a_rendre != 0:
        for valeur in dico_billet.keys():
            while somme_a_rendre >= valeur:
                dico_billet[valeur] += somme_a_rendre // valeur
                somme_a_rendre -= (somme_a_rendre // valeur) * valeur
    return dico_billet


def reponse_a(somme_a_rendre):
    """
    Permet d'afficher le nombre de billets qu'il faut rendre dans des phrases.

    Entrée: un entier qui représente la somme que l'on doit rendre
    Sortie: une chaine de caractères exprimant le nombre de billets
            que l'on doit rendre

    Version finale
    Lou, Clémence, Mayline
    """

    argent = 0
    resultat = monnaie_a(somme_a_rendre)
    texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre:\n '
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i > 2:
                texte += f"{resultat[i]} billet(s) de {i}\n"
            else:
                texte += f"{resultat[i]} pièce(s) de {i}\n"
        else:
            argent += 1
    if argent == 9:
        texte += 'rien'
    print(texte)
    return texte


def monnaie_b(a_rendre):
    """
    Permet de donner le nombre de billets qu'il faut rendre.

    Entrée: un entier représentant la somme à rendre
    Sortie: un dictionnaire avec le nombre de billets
            qu'il faut et un booléen indiquant si on a
            arrondit au nombre supérieur ou non.

    Version finale
    Lou, Clémence, Mayline

    """
    arrondit = False
    en_caisse = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    rendu = {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 2: 0}
    if a_rendre > 590:
        print("C'est trop je ne pourrais pas tous rendre mais: ")
    for valeur in en_caisse.keys():
        while a_rendre - valeur >= 0 and en_caisse[valeur] != 0:
                a_rendre -= valeur
                en_caisse[valeur] -= 1
                rendu[valeur] += 1
    if a_rendre == 1 and en_caisse[2] != 0:
        a_rendre -= 2
        en_caisse[2] -= 1
        rendu[2] += 1
        arrondit = True
    elif 100 > a_rendre > 1 and en_caisse[100] != 0:
        a_rendre -= 100
        en_caisse[100] -= 1
        rendu[100] += 1
        en_caisse[50] -= 1
        rendu[50] -= 1
        en_caisse[20] -= 1
        rendu[20] -= 1
        en_caisse[10] -= 1
        rendu[10] -= 1
        en_caisse[2] -= 5
        rendu[2] -= 5
        arrondit = True
    return rendu, arrondit


def reponse_b(somme_a_rendre):
    """
    Permet de donner une réponse avec le nombre de billets qu'il faut rendre.

    Entrée: un entier qui correspond à la somme à rendre
    Sortie: une chaine de caractères qui affiche le nombre
            de billets qu'il faut rendre ou que l'on ne peut pas rendre

    Version finale
    Lou, Clémence, Mayline

    """
    argent = 0
    resultat = monnaie_b(somme_a_rendre)
    arrondit = resultat[1]
    resultat = resultat[0]
    texte = f'Je vous dois {somme_a_rendre}, je vais donc vous rendre:\n'
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i > 2:
                texte += f"{resultat[i]} billet(s) de {i}\n"
            else:
                texte += f"{resultat[i]} pièce(s) de {i}\n"
        else:
            argent += 1
    if arrondit and somme_a_rendre <= 590:
        texte += "\nJe n'avait pas les billets nécessaires pour vous"
        texte += "rendre la monnaie exacte je vous ai donc rendu plus.\n"
    if argent == 6:
        texte += 'rien'
    print(texte)
    return texte


def monnaie_c(gallions, mornilles, noises):
    """
    Permet de donner le nombre de gallions, de noises et de mornilles
    qu'il faut rendre tout en les convertissants

    Entrée: 3 entiers représentant le nombre de gallions
            de mornilles et de noises
    Sortie: un dictionnaire avec comme clé mornilles, gallions
            et noises et le nombre qu'il faut

    Version finale
    Lou, Clémence, Mayline
    """
#   1 gallion vaut 17 mornilles , une mornille vaut 29 noises
    monnaie_a_rendre = {'gallions': 0, 'mornilles': 0, 'noises': 0}
    if noises >= 29:   # conversion noises en mornilles
        mornilles += noises // 29
        noises = noises % 29
    if mornilles >= 17:   # conversion mornilles en gallions
        gallions += mornilles // 17
        mornilles = mornilles % 17

    monnaie_a_rendre['gallions'] = gallions

    monnaie_a_rendre['mornilles'] = mornilles

    monnaie_a_rendre['noises'] = noises

    return monnaie_a_rendre


def reponse_c(gallions, mornilles, noises):
    """
    Permet d'afficher sous la forme d'un texte ce que l'on doit rendre

    Entrée: trois entiers correspondant au nombre de gallions,
            de mornilles et de noises que l'on doit rendre
    Sortie: une chaine de caractère exprimant ce que l'on doit rendre

    Version finale
    Lou, Clémence, Mayline
    """
    resultat = monnaie_c(gallions, mornilles, noises)
    texte = f"Je vous dois {gallions} gallions, {mornilles} "
    texte += f"mornilles et {noises} noises.\nJe vais donc vous rendre :\n"
    argent = 0
    for i in resultat.keys():
        if resultat[i] >= 1:
            if i == 'gallions':
                texte += f'{resultat[i]} gallions\n'
            elif i == 'mornilles':
                texte += f'{resultat[i]} mornilles\n'
            else:
                texte += f'{resultat[i]} noises.\n'
        else:
            argent += 1
    if argent == 3:
        texte += 'rien'
    print(texte)
    return texte


def exemple_a():
    """
    Permet d'exécuter les exemples donner dans le cahier des charges
    de la partie 1 et de les afficher
    """
    reponse_a(0)
    print()
    reponse_a(60)
    print()
    reponse_a(63)
    print()
    reponse_a(231)
    print()
    reponse_a(899)
    print()


def exemple_b():
    """
    Permet d'exécuter les exemples donner dans le cahier
    des charges de la partie 2 et de les afficher
    """
    reponse_b(0)
    print()
    reponse_b(8)
    print()
    reponse_b(62)
    print()
    reponse_b(231)
    print()
    reponse_b(497)
    print()
    reponse_b(842)
    print()


def exemple_c():
    """
    Permet d'exécuter les exemples donner dans le cahier
    des charges de la dernière partie et de les affciher
    """
    reponse_c(0, 0, 0)
    print()
    reponse_c(0, 0, 654)
    print()
    reponse_c(0, 23, 78)
    print()
    reponse_c(2, 11, 9)
    print()
    reponse_c(7, 531, 451)
    print()

#Programme
demande = int(input("Chez Fleury & Bott, libraire.\n"
                    "\nSi vous voulez choisir votre somme a rendre, tapez 1 \n"
                    "Si vous voulez voir les exemples tapez 2: "))
if demande == 1:
    somme_a_rendre = int(input("Combien dois-je vous rendre ? \n"))
    reponse_a(somme_a_rendre)
    print()
    demande = int(input("Si vous voulez voir les exemples tapez 2\n"))

if demande == 2:
    exemple_a()


demande = int(input("Chez Madame Giupure. Si vous voulez choisir votre "
                    "somme a rendre, tapez 1 \n Si vous voulez voir les"
                    " exemples tapez 2: "))
if demande == 1:
    a_rendre = int(input("Combien dois-je vous rendre ?\n"))
    reponse_b(a_rendre)
    print()
    demande = int(input("Si vous voulez voir les exemples tapez 2\n"))

if demande == 2:
    exemple_b()

demande = int(input("Chez Ollivandez."
                    "Si vous souhaitez choisir votre somme tapez 1 \n"
                    "Si vous voulez otbenir les resultats"
                    "des exemples tapez 2:\n "))

if demande == 1:
    gallions = int(input('Combien de gallions dois-je vous rendre ?\n'))
    mornilles = int(input("Combien de mornilles dois-je vous rendre ?\n"))
    noises = int(input('Combien de noises dois-je vous rendre ?\n'))
    reponse_c(gallions, mornilles, noises)
    print()
    demande = int(input("Si vous voulez otbenir les resultats"
                        " des exemples tapez 2: \n"))

if demande == 2:
    exemple_c()
